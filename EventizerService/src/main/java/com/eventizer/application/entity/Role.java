package com.eventizer.application.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
  ROLE_DIRECTOR, ROLE_MANAGER , ROLE_SCRUM_MASTER , ROLE_TEAM_LEAD , ROLE_TEAM_MEMBER;

  public String getAuthority() {
    return name();
  }

}
