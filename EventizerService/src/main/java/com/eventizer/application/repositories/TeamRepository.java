/**
 * 
 */
package com.eventizer.application.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eventizer.application.entity.Team;

/**
 * @author shysatya
 *
 */
@Repository
public interface TeamRepository extends JpaRepository<Team,Long>{
	
	

}
