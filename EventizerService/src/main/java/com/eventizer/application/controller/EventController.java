/**
 * 
 */
package com.eventizer.application.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.eventizer.application.annotations.AllowedForAll;
import com.eventizer.application.annotations.AllowedForBookingEvent;
import com.eventizer.application.entity.CommonUtils;
import com.eventizer.application.entity.Event;
import com.eventizer.application.entity.EventMaster;
import com.eventizer.application.entity.Response;
import com.eventizer.application.entity.SearchCriteria;
import com.eventizer.application.entity.SlotMapper;
import com.eventizer.application.entity.Status;
import com.eventizer.application.entity.StatusDTO;
import com.eventizer.application.entity.Team;
import com.eventizer.application.repositories.StatusMaster;
import com.eventizer.application.services.EventMasterService;
import com.eventizer.application.services.EventService;
import com.eventizer.application.services.TeamService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author shysatya
 *
 */
@RestController
@RequestMapping("/api/events")
@Api(tags = "EventController")
public class EventController {

	public static final Logger logger = LoggerFactory.getLogger(EventController.class);

	@Autowired
	public EventService es;

	@Autowired
	public EventMasterService ems;

	@Autowired
	public TeamService teamServ;

	@Autowired
	CommonUtils utils;

	@Autowired
	public StatusMaster stm;

	@AllowedForBookingEvent
	@PostMapping(value = "/addEvent")
	@ApiOperation(value = "${EventController.createEvent}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 422, message = "Invalid Dates for Booking") })
	public synchronized ResponseEntity<Response> createEvent(@ApiParam("Event data") @Valid @RequestBody Event event,
			UriComponentsBuilder ucBuilder, BindingResult result) throws ParseException {
		logger.info("Creating Event : {}", event);
		long key = event.getMasterKey();
		EventMaster master = ems.getEventMasterById(key);

		if (master != null) {

			logger.info("Verifying...");
			if (event.getStartTime().after(event.getEndTime()) || event.getStartTime().equals(event.getEndTime())) {
				Response resp = new Response();
				resp.setMessage("Event cannot be processed check the dates Please try again later");
				resp.setResult(Arrays.asList(event));
				resp.setStatus(false);
				return new ResponseEntity<Response>(resp, HttpStatus.CONFLICT);
			}

			Optional verify = es.findByStartTimeEndTimeAndMasterKey(event.getStartTime(), event.getEndTime(),
					event.getMasterKey());

			if (verify.isPresent()) {
				logger.info("Verified and unavailable...");
				Response resp = new Response();
				resp.setMessage("Event cannot be processed because UnAvailability of " + master.getName()
						+ " Please try again later");
				resp.setResult(Arrays.asList(event));
				resp.setStatus(false);
				return new ResponseEntity<Response>(resp, HttpStatus.CONFLICT);
			}

			logger.info("Event is processing...");

			event.setMaster(master);

			event.setStatus(Status.BOOKED);
			Team team = teamServ.getTeamById(event.getTeamKey());
			event.setTeam(team);
			es.addEvent(event);
			logger.info("Event has processed...");
		}
		if (stm.findOne(master.getId()) != null) {
			StatusDTO dto = stm.findOne(master.getId());
			dto.setDate(event.getEventDate());
			dto.setStatus(Status.BOOKED);
			stm.save(dto);
		}

		HttpHeaders headers = new HttpHeaders();
		// headers.setLocation(ucBuilder.path("/events/event/{id}").buildAndExpand(event.getId()).toUri());
		Response resp = new Response();
		resp.setMessage("Event has been Successfully created..!");
		resp.setResult(Arrays.asList(event, headers));
		resp.setStatus(true);
		return new ResponseEntity<Response>(resp, HttpStatus.CREATED);
	}

	@GetMapping(value = "/getAll")
	@ApiOperation(value = "${EventController.getAllEvents}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@AllowedForAll
	public ResponseEntity<Response> getAllEvents() {

		List<Event> events = es.getAllEvents();
		Response resp = new Response();
		resp.setMessage("Events has been successfully retrieved..!");
		resp.setResult(Arrays.asList(events));
		resp.setStatus(true);

		return new ResponseEntity<Response>(resp, HttpStatus.OK);

	}

	@RequestMapping(value = "/getByDate", method = RequestMethod.POST)
	@ApiOperation(value = "${EventController.getEventByDate}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@AllowedForAll
	public ResponseEntity<Response> getEventByDate(
			@ApiParam("Check for particular Slot") @RequestBody SlotMapper data) {

		List<Event> events = es.getEventByDate(data.getDate());
		Response resp = new Response();
		resp.setMessage("Events has been successfully retrieved..!");
		resp.setResult(Arrays.asList(events));
		resp.setStatus(true);
		return new ResponseEntity<Response>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/getByDateAndTime", method = RequestMethod.POST)
	@ApiOperation(value = "${EventController.getEventByDateAndTimeSlot}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@AllowedForAll
	public ResponseEntity<Response> getEventByDateAndTimeSlot(
			@ApiParam("Check for particular slot data") @RequestBody SlotMapper data) {

		List<Event> events = es.getEventByDateAndTime(data.getDate(), data.getStartTime(), data.getEndTime());
		Response resp = new Response();
		resp.setMessage("Events has been successfully retrieved..!");
		resp.setResult(Arrays.asList(events));
		resp.setStatus(true);
		return new ResponseEntity<Response>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/searchEvent")

	public List<Event> findAll(@RequestBody SlotMapper data) throws ParseException {
		List<SearchCriteria> params = new ArrayList<SearchCriteria>();
		if (data != null) {
			// Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
			// Matcher matcher = pattern.matcher(search + ",");
			// while (matcher.find()) {
			// params.add(new SearchCriteria(matcher.group(1),
			// matcher.group(2), matcher.group(3)));
			// }
			params.add(new SearchCriteria("eventDate", ":", data.getDate()));

			// params.add(new SearchCriteria("startTime", "<",
			// data.getStartTime()));
			// params.add(new SearchCriteria("endTime", "<",
			// data.getEndTime()));
			// params.add(new SearchCriteria("startTime", ">",
			// formatter(data.getStartTime().toString())));
			// params.add(new SearchCriteria("endTime", ">",
			// formatter(data.getEndTime().toString())));

		}
		return es.searchUser(params);
	}

	@RequestMapping(value = "/getSpecific", method = RequestMethod.POST)
	@ApiOperation(value = "${EventController.findByDateWithIntervals}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@AllowedForAll
	public ResponseEntity<Response> findByDateWithIntervals(
			@ApiParam("check the avaliable room between dates for slot data") @RequestBody SlotMapper data)
			throws ParseException {

		List<EventMaster> event = es.findByDateWithIntervals(data.getDate(), data.getEndTime(), data.getStartTime());
		List<EventMaster> events = es.findByStatusQuery(Status.AVAILABLE);

		if (event.isEmpty() && events.isEmpty()) {

			Response resp = new Response();
			resp.setMessage("Event cannot be processed because UnAvailability Please try again with different slot");
			resp.setResult(Arrays.asList(event));
			resp.setStatus(false);
			return new ResponseEntity<Response>(resp, HttpStatus.NOT_FOUND);

		}
		Response resp = new Response();
		resp.setMessage("Events has been successfully retrieved..!");
		resp.setResult(Arrays.asList(event));
		resp.setStatus(true);
		return ResponseEntity.ok(resp);
	}

	@AllowedForBookingEvent
	@RequestMapping(value = "/schedule", method = RequestMethod.POST)
	@ApiOperation(value = "${EventController.scheduleEventRecurring}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public synchronized ResponseEntity<Response> scheduleEventRecurring(
			@ApiParam("post the bulk event data") @RequestBody List<Event> events) {
		logger.info("Verifying...");
		Response resp = new Response();
		List<Event> scheduledEvents = filterLists(events);
		// es.addEvents(scheduledEvents);
		resp.setMessage("Events has been successfully Scheduled..!");
		resp.setResult(Arrays.asList(scheduledEvents));
		resp.setStatus(true);
		return ResponseEntity.ok(resp);
	}

	public List<Event> filterLists(List<Event> events) {
		try {
			List<Event> sEvents = events.parallelStream().filter(e -> es
					.findByStartTimeEndTimeAndMasterKey1(e.getStartTime(), e.getEndTime(), e.getMasterKey()) != true)
					.collect(Collectors.toList());
			System.out.println(sEvents.toString());
			sEvents.forEach(e -> {

				try {
					e.setStartTime(utils.formatter(e.getStartTime().toString()));
					e.setEndTime(utils.formatter(e.getEndTime().toString()));
					e.setEventDate(utils.formatter(e.getEventDate().toString()));
					createEvent(e, null, null);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});
			return sEvents;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getByMasterId", method = RequestMethod.GET)
	@ApiOperation(value = "${EventController.getMeetingData}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public List<Event> getMeetingData(
			@ApiParam("Post the room id for getting events by room") @RequestParam("id") long id) {

		List<Event> events = es.getAllEventsByMasterId(id);

		return events;
	}

	@RequestMapping(value = "/getByTeamId", method = RequestMethod.GET)
	@ApiOperation(value = "${EventController.getTeamData}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public List<Event> getTeamData(
			@ApiParam("Post team Id to check the events booked by the team") @RequestParam("id") long id) {

		List<Event> events = es.getAllEventsByTeamId(id);

		return events;
	}

	@RequestMapping(value = "/deleteEvent", method = RequestMethod.DELETE)
	@ApiOperation(value = "${EventController.deleteMeeting}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token or check event ID") })
	public ResponseEntity<Response> deleteEventById(
			@ApiParam("Post the event id to delete") @RequestParam("id") long id,
			@ApiParam("Post the role to delete event") @RequestParam("role") String role) {
		Event obj = es.getEventById(id);
		if (obj.getRoleString().toString().equals(role) || role.equals("[ROLE_DIRECTOR]")) {
			boolean status = es.deleteEvent(id);
			Response resp = new Response();
			resp.setMessage("Event has deleted successfully");
			resp.setStatus(status);
			resp.setResult(null);
			return ResponseEntity.ok(resp);
		}
		Response resp = new Response();
		resp.setMessage("Sorry you do not have rights to delete this event");
		resp.setStatus(false);
		resp.setResult(null);
		return new ResponseEntity<Response>(resp, HttpStatus.CONFLICT);

	}

}
