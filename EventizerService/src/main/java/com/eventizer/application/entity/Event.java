/**
 * 
 */
package com.eventizer.application.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author shysatya
 *
 */
@Entity
@Table(name = "EVENT")
@EntityListeners(AuditingEntityListener.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Event implements Serializable {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Event [eventName=" + eventName + ", eventDescription=" + eventDescription + ", eventCreatedDate="
				+ eventCreatedDate + ", eventModifiedDate=" + eventModifiedDate + ", duration=" + duration
				+ ", eventDate=" + eventDate + ", status=" + status + ", startTime=" + startTime + ", endTime="
				+ endTime + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */

	public Event() {

	}

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="recurrenceRule")
	private String recurrenceRule;

	/**
	 * @return the recurrenceRule
	 */
	public String getRecurrenceRule() {
		return recurrenceRule;
	}

	/**
	 * @param recurrenceRule the recurrenceRule to set
	 */
	public void setRecurrenceRule(String recurrenceRule) {
		this.recurrenceRule = recurrenceRule;
	}

	@Column
	@NotBlank(message = "{validationMsgSrc.blank}")
	private String eventName;

	@Column
	@NotBlank(message = "{validationMsgSrc.blank}")
	private String eventDescription;

	@Column
	@Temporal(TemporalType.DATE)
	@CreationTimestamp
	private Date eventCreatedDate;

	@Column
	@Temporal(TemporalType.DATE)
	@UpdateTimestamp
	private Date eventModifiedDate;
	
	@NotNull(message = "{validationMsgSrc.blank}")
	@Range(min=1,message="{validationMsgSrc.range}")
	private long masterKey;

	@ManyToOne
	@JoinColumn(name = "master_id")
	private EventMaster master;
	@Column
	private String roleString;

	@ManyToOne
	@JoinColumn(name = "team_id")
	private Team team;
	
	@NotNull(message = "{validationMsgSrc.blank}")
	@Range(min=1,message="{validationMsgSrc.range}")
	private long teamKey;

	private long duration;

	@JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeSerializer.class)
	private Date eventDate;

	@Column
	@Enumerated(EnumType.STRING)
	private Status status = Status.AVAILABLE;

	@JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeSerializer.class)
	private Date startTime;

	@JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeSerializer.class)
	private Date endTime;
	
	/**
	 * @return the team
	 */
	public Team getTeam() {
		return team;
	}

	/**
	 * @param team the team to set
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 * @return the roleString
	 */
	public String getRoleString() {
		return roleString;
	}

	/**
	 * @param roleString the roleString to set
	 */
	public void setRoleString(String roleString) {
		this.roleString = roleString;
	}

	/**
	 * @return the teamKey
	 */
	public long getTeamKey() {
		return teamKey;
	}

	/**
	 * @param teamKey the teamKey to set
	 */
	public void setTeamKey(long teamKey) {
		this.teamKey = teamKey;
	}

	/**
	 * @return the startTime
	 */

	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */

	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the eventDate
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * @param eventDate
	 *            the eventDate to set
	 */
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	/**
	 * @return the masterKey
	 */
	public long getMasterKey() {
		return masterKey;
	}

	/**
	 * @param masterKey
	 *            the masterKey to set
	 */
	public void setMasterKey(long masterKey) {
		this.masterKey = masterKey;
	}

	/**
	 * @return the master
	 */

	public EventMaster getMaster() {
		return master;
	}

	/**
	 * @param master
	 *            the master to set
	 */
	public void setMaster(EventMaster master) {
		this.master = master;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return eventCreatedDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.eventCreatedDate = createdDate;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return eventModifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.eventModifiedDate = modifiedDate;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName
	 *            the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the eventDescription
	 */
	public String getEventDescription() {
		return eventDescription;
	}

	/**
	 * @param eventDescription
	 *            the eventDescription to set
	 */
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	/**
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */

}
