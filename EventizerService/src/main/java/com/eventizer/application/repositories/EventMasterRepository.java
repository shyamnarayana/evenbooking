/**
 * 
 */
package com.eventizer.application.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eventizer.application.entity.EventMaster;

/**
 * @author shysatya
 *
 */
@Repository
public interface EventMasterRepository extends JpaRepository<EventMaster,Long>{
	
	List<EventMaster> findByName(String name);
	
	
	

	
}
