/**
 * 
 */
package com.eventizer.application.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eventizer.application.entity.EventMaster;
import com.eventizer.application.repositories.EventMasterRepository;

/**
 * @author shysatya
 *
 */
@Service("eventMasterService")
public class EventMasterServiceImpl implements EventMasterService{

	@Autowired
	public EventMasterRepository emr;
	
	@Override
	public List<EventMaster> getAllData() {
		
		List<EventMaster> obj = emr.findAll();
		return obj;
	}

	@Override
	public EventMaster getEventMasterById(long masterId) {
		EventMaster master = emr.findOne(masterId);
		return master;
	}

	@Override
	public boolean addEventMaster(EventMaster event) {
		
		emr.save(event);
		return true;
	}

	@Override
	public void updateEventMaster(EventMaster event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteEventMaster(int masterId) {
		// TODO Auto-generated method stub
		
	}

}
