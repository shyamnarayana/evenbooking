package com.eventizer.application.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.eventizer.application.entity.User;
import com.eventizer.application.exceptions.CustomException;
import com.eventizer.application.repositories.UserRepository;
import com.eventizer.application.security.JwtTokenProvider;
import com.fasterxml.jackson.databind.node.ArrayNode;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private AuthenticationManager authenticationManager;

	public ArrayNode signin(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
		} catch (AuthenticationException e) {
			throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	public ArrayNode signup(User user) {
		if (!userRepository.existsByUsername(user.getUsername())) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			userRepository.save(user);
			return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
		} else {
			throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	public void delete(String username) {
		userRepository.deleteByUsername(username);
	}

	public void update(User user) {
		User obj = userRepository.findByUsername(user.getUsername());
		if (obj == null) {
			throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
		} else {
			obj.setRoles(user.getRoles());
			userRepository.save(obj);
		}

	}

	public User search(String username) {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			// throw new CustomException("The user doesn't exist",
			// HttpStatus.NOT_FOUND);
			return null;
		}
		return user;
	}

	public User whoami(HttpServletRequest req) {
		return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
	}

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

}
