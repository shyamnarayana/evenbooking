/**
 * 
 */
package com.eventizer.application;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.eventizer.application.config.ResourceBuldlerConfig;
import com.eventizer.application.entity.Role;
import com.eventizer.application.entity.User;
import com.eventizer.application.services.EventService;
import com.eventizer.application.services.EventServiceImpl;
import com.eventizer.application.services.UserService;

import antlr.debug.Event;

/**
 * @author shysatya
 *
 */
@SpringBootApplication(scanBasePackages = { "com.eventizer.application.*" })
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class EventizerApplication extends SpringBootServletInitializer
		implements ServletContainerInitializer, CommandLineRunner {

	@Autowired
	UserService userService;
	

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(EventizerApplication.class, args);

	}

	@Override
	public void onStartup(Set<Class<?>> arg0, ServletContext arg1) throws ServletException {
		// TODO Auto-generated method stub

	}

	public static final DateTimeFormatter FORMATTER = ofPattern("dd-MM-yyyy HH:MM");

	public static final DateTimeFormatter TIMEFORMATTER = ofPattern("HH:MM");

	/*
	 * @Bean
	 * 
	 * @Primary public ObjectMapper serializingObjectMapper() { ObjectMapper
	 * objectMapper = new ObjectMapper();
	 * 
	 * JavaTimeModule javaTimeModule = new JavaTimeModule();
	 * javaTimeModule.addSerializer(LocalDateTime.class, new
	 * LocalDateSerializer());
	 * javaTimeModule.addDeserializer(LocalDateTime.class, new
	 * LocalDateDeserializer()); objectMapper.registerModule(javaTimeModule);
	 * return objectMapper; }
	 */
	  @Bean
	    public WebMvcConfigurer corsConfigurer() {
	        return new WebMvcConfigurerAdapter() {
	            @Override
	            public void addCorsMappings(CorsRegistry registry) {
	                registry.addMapping("/**");
	            }
	        };
	    }
	    
	    @Bean
	    public FilterRegistrationBean filterRegistrationBean() {
	        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
	        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
	        characterEncodingFilter.setForceEncoding(true);
	        characterEncodingFilter.setEncoding("UTF-8");
	        registrationBean.setFilter(characterEncodingFilter);
	        return registrationBean;
	    }
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EventizerApplication.class);
	}

	@Bean
	public javax.validation.Validator getValidator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		validator.setValidationMessageSource(ResourceBuldlerConfig.messageSource());
		return validator;
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Override
	public void run(String... params) throws Exception {
		
		User admin = new User();
		admin.setUsername("admin");
		admin.setPassword("admin");
		admin.setEmail("admin@email.com");
		admin.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_TEAM_MEMBER)));
		if (userService.search(admin.getUsername().toString()) == null) {
			userService.signup(admin);
		}else{
			userService.delete(admin.getUsername().toString());
			userService.signup(admin);
		}
		User client = new User();
		client.setUsername("client");
		client.setPassword("client");
		client.setEmail("client@email.com");
		client.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_TEAM_MEMBER)));
		if (userService.search(client.getUsername().toString()) == null) {
			userService.signup(client);
		}else{
			userService.delete(client.getUsername().toString());
			userService.signup(client);
		}
		User director = new User();
		director.setUsername("genesis");
		director.setPassword("epam");
		director.setEmail("genesis@email.com");
		director.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_MANAGER)));
		if (userService.search(director.getUsername().toString()) == null) {
			userService.signup(director);
		}else{
			userService.delete(director.getUsername().toString());
			userService.signup(director);
		}
		User manager = new User();
		manager.setUsername("epamclientdir");
		manager.setPassword("epamclientdir");
		manager.setEmail("epamclientdir@email.com");
		manager.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_DIRECTOR)));
		if (userService.search(manager.getUsername().toString()) == null) {
			userService.signup(manager);
		}else{
			userService.delete(manager.getUsername().toString());
			userService.signup(manager);
		}
		User scrummaster = new User();
		scrummaster.setUsername("scurm123");
		scrummaster.setPassword("scrum123");
		scrummaster.setEmail("scrum123@email.com");
		scrummaster.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_SCRUM_MASTER)));
		if (userService.search(scrummaster.getUsername().toString()) == null) {
			userService.signup(scrummaster);
		}else{
			userService.delete(scrummaster.getUsername().toString());
			userService.signup(scrummaster);
		}

	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}

}
