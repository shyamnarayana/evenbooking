/**
 * 
 */
package com.eventizer.application.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eventizer.application.entity.Event;

/**
 * @author shysatya
 *
 */
@Repository("eventJpaRepo")
public interface EventJpaRepository  extends JpaRepository<Event,Long>{

	List<Event> findByEventName(String name);
	List<Event> findByEventCreatedDate(Date date);
	List<Event> findByEventDescription(String desc);
	List<Event> findByEventDate(Date date);
	List<Event> findByEventDateAndStartTimeAndEndTime(Date date,Date startTime,Date endTime);
	List<Event> findByMasterKey(long id);
	List<Event> findByTeamKey(long id);

}
