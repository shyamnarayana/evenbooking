package com.eventizer.application.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.eventizer.application.entity.Event;
import com.eventizer.application.entity.EventMaster;
import com.eventizer.application.entity.SearchCriteria;
import com.eventizer.application.entity.Status;

@Component
public interface EventService {

	List<Event> getAllEvents();

	Event getEventById(long eventId);

	boolean addEvent(Event event);	

	void updateEvent(Event event);

	boolean deleteEvent(long EventId);

	List<Event> getEventByDate(Date localDateTime);
	
	List<Event> getEventByDateAndTime(Date date,Date startTime,Date endTime);
	
	 public List<Event> searchUser(List<SearchCriteria> params) throws ParseException;

	List<EventMaster> findByDateWithIntervals(Date date, Date endTime, Date startTime) throws ParseException;
	
	List<EventMaster> findByStatusQuery(Status available);
	
	Optional findByStartTimeEndTimeAndMasterKey(Date startTime,Date endTime,long masterKey) throws ParseException;
	
	boolean addEvents(List<Event> event);

	boolean findByStartTimeEndTimeAndMasterKey1(Date startTime, Date endTime, long masterKey);

	List<Event> getAllEventsByMasterId(long id);

	List<Event> getAllEventsByTeamId(long id);	

	

}
