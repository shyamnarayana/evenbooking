/**
 * 
 */
package com.eventizer.application.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eventizer.application.entity.Team;
import com.eventizer.application.repositories.TeamRepository;



/**
 * @author shysatya
 *
 */
@Service
public class TeamServiceImpl implements TeamService{
	
	@Autowired
	TeamRepository teamRepo;

	@Override
	public List<Team> getAllData() {
		
		return teamRepo.findAll();
	}

	@Override
	public Team getTeamById(long masterId) {
		Team team = teamRepo.findOne(masterId);
		return team;
	}

	@Override
	public boolean addTeam(Team team) {
		
		teamRepo.save(team);
		return true;
	}

	@Override
	public void updateTeam(Team team) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteTeam(int teamId) {
		// TODO Auto-generated method stub
		
	}

	

}
