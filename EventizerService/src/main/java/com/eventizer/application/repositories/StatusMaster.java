/**
 * 
 */
package com.eventizer.application.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eventizer.application.entity.StatusDTO;

/**
 * @author shysatya
 *
 */
@Repository
public interface StatusMaster extends JpaRepository<StatusDTO,Long>{
	
	

}
