package com.eventizer.application.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eventizer.application.annotations.AllowedForAll;
import com.eventizer.application.annotations.AllowedForDirector;
import com.eventizer.application.entity.Response;
import com.eventizer.application.entity.User;
import com.eventizer.application.entity.UserDataDTO;
import com.eventizer.application.entity.UserResponseDTO;
import com.eventizer.application.services.UserService;
import com.fasterxml.jackson.databind.node.ArrayNode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/users")
@Api(tags = "UserController")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	Response resp;

	@PostMapping("/signin")
	@ApiOperation(value = "${UserController.signin}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 422, message = "Invalid username/password supplied") })
	public ResponseEntity<Response> login(//
			@ApiParam("Username") @RequestParam String username, //
			@ApiParam("Password") @RequestParam String password) {
		ArrayNode token = userService.signin(username, password);
		if (token != null) {
			resp.setMessage("Token Created SuccessFully");
			resp.setStatus(true);
			resp.setResult(Arrays.asList(username, token));
			return ResponseEntity.ok(resp);
		} else {
			resp.setMessage("Error in generating the token..!");
			resp.setStatus(false);
			resp.setResult(Arrays.asList(username, token));
			return ResponseEntity.ok(resp);
		}

	}

	@PostMapping("/signup")
	@ApiOperation(value = "${UserController.signup}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 422, message = "Username is already in use"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<Response> signup(@ApiParam("Signup User") @RequestBody UserDataDTO user) {
		ArrayNode token = userService.signup(modelMapper.map(user, User.class));
		if (token != null) {
			resp.setMessage("User Created SuccessFully Find the Token...!");
			resp.setStatus(true);
			resp.setResult(Arrays.asList(user.getUsername(), user.getEmail(), token));
			return ResponseEntity.ok(resp);
		} else {
			resp.setMessage("Error creating the User..!");
			resp.setStatus(false);
			resp.setResult(Arrays.asList(user, token));
			return ResponseEntity.ok(resp);
		}

	}

	@DeleteMapping(value = "/{username}")
	@AllowedForDirector
	@ApiOperation(value = "${UserController.delete}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 404, message = "The user doesn't exist"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public String delete(@ApiParam("Username") @PathVariable String username) {
		userService.delete(username);
		return username;
	}

	@GetMapping(value = "/{username}")
	@AllowedForDirector
	@ApiOperation(value = "${UserController.search}", response = UserResponseDTO.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 404, message = "The user doesn't exist"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public UserResponseDTO search(@ApiParam("Username") @PathVariable String username) {
		return modelMapper.map(userService.search(username), UserResponseDTO.class);
	}

	@GetMapping(value = "/me")
	@AllowedForAll
	@ApiOperation(value = "${UserController.me}", response = UserResponseDTO.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public UserResponseDTO whoami(HttpServletRequest req) {
		return modelMapper.map(userService.whoami(req), UserResponseDTO.class);
	}

	@PutMapping
	@AllowedForDirector
	@ApiOperation(value = "${UserController.update}")
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 404, message = "The user doesn't exist"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<Response> update(@RequestBody User user) {
		userService.update(user);
		resp.setMessage("Permissions are successfully updated");
		resp.setStatus(true);
		resp.setResult(Arrays.asList(user));
		return ResponseEntity.ok(resp);
	}

	@GetMapping(value = "/getUsers")
	@AllowedForDirector
	@ApiOperation(value = "${UserController.getUsers}", response = UserResponseDTO.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public List<UserResponseDTO> getAllUsers() {
		return modelMapper.map(userService.getAllUsers(), new TypeToken<List<UserResponseDTO>>() {}.getType());
	}

}
