/**
 * 
 */
package com.eventizer.application.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.eventizer.application.annotations.AllowedForDirector;
import com.eventizer.application.annotations.AllowedForManager;
import com.eventizer.application.annotations.AllowedForMasterAndTeam;
import com.eventizer.application.annotations.AllowedForScrumMaster;
import com.eventizer.application.annotations.AllowedForTeamLead;
import com.eventizer.application.entity.EventMaster;
import com.eventizer.application.entity.Response;
import com.eventizer.application.entity.Status;
import com.eventizer.application.entity.StatusDTO;
import com.eventizer.application.repositories.StatusMaster;
import com.eventizer.application.services.EventMasterService;

import io.swagger.annotations.Api;

/**
 * @author shysatya
 *
 */
@RestController
@RequestMapping("/api/masters")
@Api(tags = "EventMasterController")
public class EventMasterController {

	public static final Logger logger = LoggerFactory.getLogger(EventMasterController.class);

	@Autowired
	public EventMasterService ems;

	@Autowired
	public StatusMaster stm;
	
	@AllowedForMasterAndTeam
	@RequestMapping(value = "/addMaster", method = RequestMethod.POST)
	public ResponseEntity<Response> addMaster(@Valid @RequestBody EventMaster master, UriComponentsBuilder ucBuilder,
			BindingResult result) {

		logger.info("Creating master : {}", master);
		boolean asserted = ems.addEventMaster(master);
		StatusDTO dto = new StatusDTO(master.getEventCreatedDate(), Status.AVAILABLE, master.getId());
		dto.setStatusMaster(master);
		stm.save(dto);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/masters/master/{id}").buildAndExpand(master.getId()).toUri());
		Response resp = new Response();
		resp.setMessage("Master has added successfully");
		resp.setStatus(asserted);
		resp.setResult(Arrays.asList(master));
		return new ResponseEntity<Response>(resp, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<Response> retrieveMasterData() {

		logger.info("Fetching all Master Records");
		List<EventMaster> obj = ems.getAllData();
		Response resp = new Response();
		resp.setMessage("Records are retrived Successfully");
		resp.setStatus(true);
		resp.setResult(obj);
		return new ResponseEntity<Response>(resp, HttpStatus.ACCEPTED);

	}

}
