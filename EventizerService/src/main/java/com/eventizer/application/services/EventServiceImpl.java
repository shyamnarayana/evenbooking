/**
 * 
 */
package com.eventizer.application.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.eventizer.application.entity.CommonUtils;
import com.eventizer.application.entity.Event;
import com.eventizer.application.entity.EventMaster;
import com.eventizer.application.entity.SearchCriteria;
import com.eventizer.application.entity.Status;
import com.eventizer.application.repositories.EventJpaRepository;

/**
 * @author shysatya
 *
 */
@Service("eventService")
public class EventServiceImpl implements EventService {

	@Autowired
	@Qualifier(value = "eventJpaRepo")
	EventJpaRepository er;

	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	public CommonUtils utils;
	
	
	@Override
	public List<Event> getAllEvents() {

		return er.findAll();
	}

	@Override
	public Event getEventById(long eventId) {
		Event obj = er.findOne(eventId);
		return obj;
	}

	@Override
	public boolean addEvent(Event event) {
		er.save(event);
		return true;
	}

	@Override
	public void updateEvent(Event event) {

	}

	@Override
	public boolean deleteEvent(long EventId) {
		er.delete(EventId);
		return true;
	}

	@Override
	public List<Event> getEventByDate(Date date) {
		List<Event> obj = er.findByEventDate(date);

		return obj;
	}

	@Override
	public List<Event> getEventByDateAndTime(Date date, Date startTime, Date endTime) {

		List<Event> obj = er.findByEventDateAndStartTimeAndEndTime(date, startTime, endTime);

		return obj;
	}

	public List<Event> searchUser(List<SearchCriteria> params) throws ParseException {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Event> query = builder.createQuery(Event.class);
		Root r = query.from(Event.class);

		Predicate predicate = builder.conjunction();

		for (SearchCriteria param : params) {
			// 2018-01-13 14:12:00.0
			System.out.println(param.getValue());
			if (param.getOperation().equalsIgnoreCase(">")) {
				predicate = builder.and(predicate, builder.greaterThanOrEqualTo(r.get(param.getKey()),
						utils.formatter(param.getValue().toString())));
			} else if (param.getOperation().equalsIgnoreCase("<")) {
				predicate = builder.and(predicate,
						builder.lessThanOrEqualTo(r.get(param.getKey()), utils.formatter(param.getValue().toString())));
			} else if (param.getOperation().equalsIgnoreCase(":")) {
				if (r.get(param.getKey()).getJavaType() == String.class) {
					predicate = builder.and(predicate,
							builder.like(r.get(param.getKey()), "%" + param.getValue() + "%"));
				} else {
					predicate = builder.and(predicate,
							builder.equal(r.get(param.getKey()), utils.formatter(param.getValue().toString())));
				}
			}
		}
		query.where(predicate);
		List<Event> result = entityManager.createQuery(query).getResultList();
		return result;
	}

	@Override
	public List<EventMaster> findByDateWithIntervals(Date date, Date endTime, Date startTime) throws ParseException {
		Query query = entityManager.createQuery(
				"SELECT eM FROM EventMaster eM WHERE  eM.id NOT IN(SELECT masterKey from Event e where e.startTime BETWEEN  :startTime AND :endTime"
						+ " OR e.endTime BETWEEN :startTime AND :endTime)")

				.setParameter("startTime", utils.formatter(startTime.toString()))
				.setParameter("endTime", utils.formatter(endTime.toString()));
		List<EventMaster> obj = query.getResultList();
		return obj;
	}

	public List<EventMaster> findByStatusQuery(Status status) {

		Query query = entityManager
				.createQuery(
						"SELECT e from EventMaster e where e.id IN (SELECT masterKey FROM StatusDTO st where st.status = :status)")
				.setParameter("status", status);
		List<EventMaster> obj = query.getResultList();
		return obj;

	}

	@Override
	public  Optional findByStartTimeEndTimeAndMasterKey(Date startTime, Date endTime, long masterKey)
			throws ParseException {
		
		
		Query query = entityManager
				.createQuery("SELECT masterKey from Event e where e.startTime BETWEEN  :startTime AND :endTime"
						+ " OR e.endTime BETWEEN :startTime AND :endTime")
				.setParameter("startTime", utils.formatter(startTime.toString()))
				.setParameter("endTime", utils.formatter(endTime.toString()));
		List obj = query.getResultList();
		Optional optRef = utils.filterListWithValue(obj, masterKey);
		return optRef;
	}
	@Override
	public  boolean findByStartTimeEndTimeAndMasterKey1(Date startTime, Date endTime, long masterKey)
			 {

		Query query;
		Optional optRef =null;
		try {
			query = entityManager
					.createQuery("SELECT masterKey from Event e where e.startTime BETWEEN  :startTime AND :endTime"
							+ " OR e.endTime BETWEEN :startTime AND :endTime")
					.setParameter("startTime", utils.formatter(startTime.toString()))
					.setParameter("endTime", utils.formatter(endTime.toString()));
			List obj = query.getResultList();
			optRef = utils.filterListWithValue(obj, masterKey);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return optRef.isPresent();
	}

	@Override
	public boolean addEvents(List<Event> events) {
		er.save(events);
		return true;
	}

	@Override
	public List<Event> getAllEventsByMasterId(long id) {
		List<Event> events = er.findByMasterKey(id);
		return events;
	}
	@Override
	public List<Event> getAllEventsByTeamId(long id) {
		List<Event> events = er.findByTeamKey(id);
		return events;
	}
	

}
